import os
import os.path
import csv
import sys
import re

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
DATA_LOG_FILE = os.path.join(PROJECT_DIRECTORY, "../../data/datalog.csv")
WP_LOG_FILE = os.path.join(PROJECT_DIRECTORY, "../work-products-log.csv")
SCRIPT_FILE = "run.sh"
TMP_PREFIX = "tmp_"
# Allow for data UIDs of the form D001, d1, D02, ...
DATA_UID_PATTERN = re.compile("^(D|d)([0-9]+)$")

# Python 2 and 3 compatibility regarding input()
try:
    input = raw_input
except NameError:
    pass


def script_setup(wp_prefix="WP", out_prefix="out_", tmp_prefix=TMP_PREFIX):
    return """#!/bin/sh

echo "=== SETUP Parameters ==="
echo "for work product with id: {{ cookiecutter.work_product_uid }}"
echo "about: {{ cookiecutter.description }}"

# * UID for Work product (WP) *
WP='{{ cookiecutter.work_product_uid }}'
echo "WP = "$WP
# Prepare a common prefix including WP ID
PREFIX='%(wp)s'$WP'_'
echo "PREFIX = "$PREFIX
# Mark all generated output files (to ease cleanup)
OUTPREFIX=$PREFIX'%(out)s'
echo "OUTPREFIX = "$OUTPREFIX
# Mark all temporary files (to ease cleanup)
TMPPREFIX='%(tmp)s'
echo "TMPPREFIX = "$TMPPREFIX
""" % {"wp": wp_prefix, "out": out_prefix, "tmp": tmp_prefix}


def script_data_sources(data):
    result = """echo "=== DATA SOURCES ==="
# Clarify what data you use (ID, Filename) and setup according shell variables and a local filename.
"""
    counter = 0
    for uid in data.keys():
        filename = data[uid]
        localname = uid + "_" + filename
        result += """SRC_%(id)s_ID="%(id)s"
SRC_%(id)s_FILENAME="%(filename)s"
SRC_%(id)s=$TMPPREFIX"%(localname)s"
echo "Data %(counter)s = "$SRC_%(id)s_ID" - "$SRC_%(id)s_FILENAME" - "$SRC_%(id)s
""" % {"counter": counter, "id": uid, "filename": filename, "localname": localname}
        counter += 1
    return result


def script_preparation(data):
    result = """echo "=== PREPARATION STEPS ==="
echo "* Clean up output files (intermediate and final)"
rm $OUTPREFIX*
rm $TMPPREFIX*

echo "* Copy source data to local directory"
# Principle 1: Space is cheap, confusion is expensive.
# COPY data from source directory and MODIFY it only HERE!
"""
    for uid in data.keys():
        result += """cp ../../data/$SRC_%(id)s_ID/$SRC_%(id)s_FILENAME $SRC_%(id)s
""" % {"id": uid}
    return result


def script_run_analysis():
    return """echo "=== RUN ANALYSIS ==="
# E.g. running XSLT scripts to extract data
# Prepare for a conversion to CSV
# Creating intermediary (temporary) helping files
# Remember to echo your steps to enable logging and being able to follow the analysis

"""


def script_finalize():
    return """echo "=== FINALIZE ==="
# Report on end of the process
# What (non-automatic) steps now need to be taken?
echo "=== DONE ==="
"""


def parse_datalogcsv(datalog_path):
    datalog = {}
    if os.path.isfile(datalog_path):
        with open(datalog_path, 'r', newline='') as data_csv:
            datalog_reader = csv.reader(data_csv, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for row in datalog_reader:
                if len(row)>0:
                    uid = row[0]
                    filename = row[1]
                    if uid in datalog.keys():
                        # UID is already present as a key, i.e. has been seen before!
                        print("WARNING: Your datalog is inconsistent! The Data UID %s is found more than once " +
                              "in your datalog.csv! Aborting investigation." % uid)
                        return {}
                    else:
                        datalog[uid] = filename
    return datalog


if __name__ == '__main__':
    # How to ensure, that the project directory does not yet exist - validate immediately otherwise tedious!
    # Add to work-products-log.csv if requested (presumes finalized rows)
    if '{{ cookiecutter.add_to_wplog }}'.lower() in ('y', 'yes'):
        with open(WP_LOG_FILE, 'a') as csv_file:
            datalog_writer = csv.writer(csv_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            datalog_writer.writerow(['{{cookiecutter.work_product_uid}}', '{{cookiecutter.description}}',
                                     '{{cookiecutter.built_by}}', '{{cookiecutter.delivered_to}}',
                                     '{{cookiecutter.delivered_on}}'])
    # Go on with user questions when asked for a run script skeleton
    if '{{ cookiecutter.create_run_script_skeleton }}'.lower() in ('y', 'yes'):
        print(DATA_LOG_FILE)
        data_log = parse_datalogcsv(DATA_LOG_FILE)
        print(str(data_log.keys()))
        sys.stdout.flush()
        go_on = True
        data = {}
        while go_on:
            print("Enter Data UID (or ENTER to finish data source capture): ")
            sys.stdout.flush()   # Needed at least at Windows to ensure question is printed
            valid = False
            while not valid:
                uid = input()
                if uid == "":
                    valid = True
                else:
                    valid = DATA_UID_PATTERN.match(uid)
                    if not valid:
                        print("Not admissible data: format should be 'D[Digits]' or '' to end data capture! " +
                              "Please try again")
                        sys.stdout.flush()
                    else:
                        # Validate against datalog.csv
                        if data_log.keys() and not(uid in data_log.keys()):
                            print("The specified Data UID does not exist yet in your datalog. " +
                                  "Do you still want to use it? (y/n)")
                            sys.stdout.flush()
                            yesno = input()
                            valid = yesno.lower() in ["y", "yes"]
            go_on = uid != ""
            if go_on:
                # If possible derive filename from datalog.csv otherwise ask for it
                if data_log.keys() and (uid in data_log.keys()):
                    filename = data_log[uid]
                    print("Using filename '%s'." % filename)
                else:
                    print("Enter filename (Data UID not present in datalog.csv): ")
                    sys.stdout.flush()
                    filename = input()
                data[uid] = filename
        # Create script content optionally based on referred data.
        with open(SCRIPT_FILE, 'w') as script_file:
            script_file.write(script_setup())
            script_file.write(script_data_sources(data))
            script_file.write(script_preparation(data))
            script_file.write(script_run_analysis())
            script_file.write(script_finalize())
    else:
        os.remove(os.path.join(PROJECT_DIRECTORY, SCRIPT_FILE))
